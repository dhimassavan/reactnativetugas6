import { View, Text } from 'react-native'
import React from 'react'
 import Api from './src/screen/Api'
import Routing from './src/simpleNetworking/Routing'
import Home from './src/simpleNetworking/Home'
const App = () => {
  return (
    <View style={{flex:1}}>
      <Routing/>
    </View>
  )
}

export default App