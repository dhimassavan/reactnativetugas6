// import React, {useEffect, useState} from 'react';
// import {
//   View,
//   Text,
//   TextInput,
//   TouchableOpacity,
//   StyleSheet,
// } from 'react-native';
// import Icon from 'react-native-vector-icons/AntDesign';
// import {BASE_URL, TOKEN} from './url';
// import Modal from "react-native-modal";
// import Axios from 'axios'


// const AddData = ({navigation, route}) => {
//     const deleteData = async () => {
//         const body = [
//          {
//            "_uuid": dataMobil._uuid,
//          }
//         ]
//         try {
//           const response = await fetch(`${BASE_URL}mobil`, {
//             method: "DELETE",
//             headers: {
//              "Content-Type": "application/json",
//              "Authorization": TOKEN
//             },
//             body: JSON.stringify(body)
//           });
            
//           const result = await response.json();
//           alert('Data Mobil berhasil dihapus')
//           navigation.navigate('Home')
//         } catch (error) {
//            console.error("Error:", error);
//         }
//       }
      
//   const editData = async () => {
//     const body = [
//       {
//         _uuid: dataMobil._uuid,
//         title: namaMobil,
//         harga: hargaMobil,
//         totalKM: totalKM,
//         unitImage:
//           'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
//       },
//     ];
//     try {
//       const response = await fetch(`${BASE_URL}mobil`, {
//         method: 'PUT',
//         headers: {
//           'Content-Type': 'application/json',
//           Authorization: TOKEN,
//         },
//         body: JSON.stringify(body),
//       });

//       const result = await response.json();
//       console.log('Success:', result);
//       alert('Data Mobil berhasil dirubah');
//       navigation.navigate('Home')
//     } catch (error) {
//       console.error('Error:', error);
//     }
//   };

//   useEffect(() => {
//     if (route.params) {
//       const data = route.params;
//       setNamaMobil(data.title);
//       setTotalKM(data.totalKM);
//       setHargaMobil(data.harga);
//       setHargaMobil(data.unitImage);
//     }
//   }, []);
//   const [namaMobil, setNamaMobil] = useState('');
//   const [totalKM, setTotalKM] = useState('');
//   const [hargaMobil, setHargaMobil] = useState('');
//   const [imageMobil, setImageMobil] = useState('');
//   var dataMobil = route.params;
//   const postData = async () => {
//     const body = [
//       {
//         title: namaMobil,
//         harga: hargaMobil,
//         totalKM: totalKM,
//         unitImage:
//           'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
//       },
//     ];
//     const options = {
//       headers:{
//         'Content-Type': 'application/json',
//          Authorization: TOKEN,
//       }
//     }
//     Axios.post(`${BASE_URL}mobil`,body,options)
//         .then((response)=> {
//           console.log('success',response);
//           if(response.status === 200 || response.status === 201){
//             alert('Data Mobil berhasil ditambahkan');
//             navigation.navigate('Home')
//           }
//         })
//         .catch((error)=>{
//           console.log('error',error);
//         })
      
//   };const [isNamaMobilValid, setIsNamaMobilValid] = useState(true);
//   const [isTotalKMValid, setIsTotalKMValid] = useState(true);
//   const [isHargaMobilValid, setIsHargaMobilValid] = useState(true);
//   const [isImageMobilValid, setIsImageMobilValid] = useState(true);

//   return (
//     <View style={{flex: 1, backgroundColor: '#fff'}}>
//       <Modal animationType = {"slide"} transparent = {true} isVisible={true} style={{backgroundColor:'#FFFFFF'}}>
//       <View
//         style={{
          
//           flexDirection: 'row',
//           alignItems: 'center',
//         }}>
//         <TouchableOpacity
//           onPress={() => navigation.goBack()}
//           style={{
//             width: '10%',
//             justifyContent: 'center',
//             alignItems: 'center',
//             paddingVertical: 10,
//           }}>
//           <Icon name="arrowleft" size={20} color="#000" />
//         </TouchableOpacity>
//         <Text style={{fontSize: 16, fontWeight: 'bold', color: '#000'}}>
//           {dataMobil ? 'Ubah data' : 'Tambah data'}
//         </Text>
//       </View>
//       <View
//         style={{
//           width: '100%',
//           padding: 15,
//         }}>
//         <View>
//           <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
//             Nama Mobil
//           </Text>
//           <TextInput
//             placeholder="Masukkan Nama Mobil"
//             style={[styles.txtInput, !isNamaMobilValid && styles.invalidInput]}
//             value={namaMobil}
//             onChangeText={text => setNamaMobil(text)}
//             onBlur={() => setIsNamaMobilValid(!!namaMobil)}
//           />
//         </View>
//         <View style={{marginTop: 20}}>
//           <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
//             Total Kilometer
//           </Text>
//           <TextInput
//             placeholder="contoh: 100 KM"
//             style={[styles.txtInput, !isTotalKMValid && styles.invalidInput]}
//             onChangeText={text => setTotalKM(text)}
//             value={totalKM}
//             onBlur={() => setIsTotalKMValid(!!totalKM)}
//           />
//         </View>
//         <View style={{marginTop: 20}}>
//           <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
//             Harga Mobil
//           </Text>
//           <TextInput
//             placeholder="Masukkan Harga Mobil"
//             style={[styles.txtInput, !isHargaMobilValid && styles.invalidInput]}
//             keyboardType="number-pad"
//             onChangeText={text => setHargaMobil(text)}
//             value={hargaMobil}
//             onBlur={() => setIsHargaMobilValid(!!hargaMobil)}
//           />
//         </View>
//         <View style={{marginTop: 20}}>
//           <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
//             Link Gambar 
//           </Text>
//           <TextInput
//             placeholder="Masukkan Link gambar"
//             style={[styles.txtInput, !isImageMobilValid && styles.invalidInput]}
//             keyboardType="number-pad"
//             onChangeText={text => setImageMobil(text)}
//             value={imageMobil}
//             onBlur={() => setIsImageMobilValid(!!imageMobil)}
//           />
//         </View>
//         <TouchableOpacity
//           style={styles.btnAdd}
//           onPress={() => {
//             if (namaMobil && totalKM && hargaMobil) {
//               dataMobil ? editData() : postData();
//             } else {
//               setIsNamaMobilValid(!!namaMobil);
//               setIsTotalKMValid(!!totalKM);
//               setIsHargaMobilValid(!!hargaMobil);
//             }
//           }}>
//           <Text style={{color: '#fff', fontWeight: '600'}}>
//             {dataMobil ? 'Ubah data' : 'Tambah data'}
//           </Text>
//         </TouchableOpacity>
//         { dataMobil ?
//             <TouchableOpacity style={[styles.btnAdd, {backgroundColor: 'red'}]} onPress={deleteData}>
//           <Text style={{color: '#fff', fontWeight: '600'}}>Hapus Data</Text>
          
//         </TouchableOpacity>
//         :null
//         } 
//       </View>
//       </Modal>

//     </View>
//   );
// };

// const styles = StyleSheet.create({
//   invalidInput: {
//     borderColor: 'red',
//   },
//   btnAdd: {
//     marginTop: 20,
//     width: '100%',
//     paddingVertical: 10,
//     borderRadius: 6,
//     backgroundColor: '#689f38',
//     justifyContent: 'center',
//     alignItems: 'center',
//   },
//   txtInput: {
//     marginTop: 10,
//     width: '100%',
//     borderRadius: 6,
//     paddingHorizontal: 10,
//     borderColor: '#dedede',
//     borderWidth: 1,
//   },
// });

// export default AddData;
