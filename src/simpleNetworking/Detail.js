import {View, Text, Image, ImageBackground , TouchableOpacity, StyleSheet} from 'react-native';
import React, { useEffect, useState } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {BASE_URL, TOKEN} from './url'
import { useIsFocused } from '@react-navigation/native';
const Detail = ({navigation,route}) => {
    const [namaMobil,setNamaMobil] =useState('');
    const [totalKM,setTotalKM] =useState('');
    const [hargaMobil,setHargaMobil] =useState('');
    const [unitImage,setUnitImage] =useState('');
    var dataDetail= route.params;
    console.log('data detail' ,dataDetail)
    // useEffect(() => {
    //     setHargaMobil(dataDetail.hargaMobil);
    //     setUnitImage(dataDetail.unitImage);
    //   }, []);
    
  return (
    <View style={{flex: 1}}>
      <Image
        source={{uri: dataDetail.unitImage}}
        style={{width: '100%', height: 316, alignItems:'flex-end'}}>
      </Image>
      <View style={{alignItems:'center',marginTop:10}}>
        <Text style={{fontWeight:'bold',fontSize:24}}>Nama Mobil : {dataDetail.title}</Text>
        <Text style={{fontWeight:'bold',fontSize:24}}>Harga Mobil : {dataDetail.harga}</Text>
        <Text style={{fontWeight:'bold',fontSize:24}}>Jumlah KM : {dataDetail.totalKM}</Text>
      </View>
      <TouchableOpacity onPress={()=>navigation.navigate('AddData',dataDetail)}
          style={styles.btnAdd}>
          <Text style={{color: '#fff', fontWeight: '600',}}>
            Ubah Data
          </Text>
        </TouchableOpacity>
      

      
    </View>
  );
};
const styles = StyleSheet.create({
    btnAdd: {
      marginTop: 20,
      width: '100%',
      paddingVertical: 10,
      borderRadius: 6,
      backgroundColor: '#689f38',
      justifyContent: 'center',
      alignItems: 'center',
    },
    txtInput: {
      marginTop: 10,
      width: '100%',
      borderRadius: 6,
      paddingHorizontal: 10,
      borderColor: '#dedede',
      borderWidth: 1,
    },
  });
export default Detail;
