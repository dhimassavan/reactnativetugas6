import { View, Text ,FlatList, TouchableOpacity, Image,ActivityIndicator,Modal, RefreshControl,ScrollView} from 'react-native'
import React, { useEffect, useState } from 'react'
import {BASE_URL, TOKEN} from './url'
import  Icon  from 'react-native-vector-icons/AntDesign'
import { useIsFocused } from '@react-navigation/native'
import Axios from 'axios'




const Home = ({navigation}) => {
const [dataMobil, setDataMobil]= useState ({} )
const [showModal, setShowModal] = useState(false)
const isFocused = useIsFocused()
  useEffect(() => {
    getDataMobil()
    
},[isFocused])
const getDataMobil = async () => {
    setShowModal(true)
 
Axios.get(`${BASE_URL}mobil`,{
    headers: {
    "Content-Type": "application/json",
    "Authorization": TOKEN
    }
})
    .then((response) => {
        console.log('res get mobil', response);
        if(response.status === 200){
            setDataMobil(response.data.items);
        } else{
            if(response.status === 401 || response.status === 402){
                // status 401 maka user harus login kembali
                return false
            }
            if(response.status === 500 ){
                // status 500 server eror
                return false
            }
        }
    })
    .catch((error) => {
        console.log('error get mobil', error);
    })
    
    setTimeout(() => {
        setShowModal(false)
        
    },3000)
}
console.log('datamobil :',dataMobil)
const convertCurrency = (nominal=0, currency) => {
  let rupiah = '';
  const nominalref = nominal.toString().split('').reverse().join('');
  for (let i = 0; i < nominalref.length; i++) {
      if (i % 3 === 0) {
          rupiah += nominalref.substr(i, 3) + '.';
      }
  }

  if (currency) {
      return (
          currency +
          rupiah
          .split('', rupiah.length - 1)
          .reverse()
          .join('')
      );
  } else {
      return rupiah
          .split('', rupiah.length - 1)
          .reverse()
          .join('');
  }
}
const [refreshing, setRefreshing] = React.useState(false);

const onRefresh = React.useCallback(() => {
  setRefreshing(true);
  setTimeout(() => {
    setRefreshing(false);
  }, 2000);
}, []);

  return (
    <View style={{flex:1,backgroundColor: '#FFFFFF'}}>
        <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
      
        <Modal visible={showModal}
        transparent={true} animationIn='fade'>
            <View style={{width:'100%',height:'100%',backgroundColor:'white', alignContent:'center', justifyContent:'center',alignItems:'center'}}>
                <View style={{padding:20,borderRadius:10,backgroundColor:'white'}}>
                    <ActivityIndicator size='large' color='#00ff00'  />
                </View>
            </View>
        </Modal>
        
           <Text style={{fontWeight: 'bold', fontSize: 20, margin: 15, color: '#000'}}>Home screen</Text>
           <FlatList
           
               data={dataMobil}
               keyExtractor={(item, index) => index.toString()}
               renderItem={({item, index}) => (
                   <TouchableOpacity
                       onPress={() => navigation.navigate('Detail',item)}
                       activeOpacity={0.8}
                       style={{
                           width: '90%',
                           alignSelf: 'center',
                           marginTop: 15,
                           borderColor: '#dedede',
                           borderWidth: 1,
                           borderRadius: 6,
                           padding: 12,
                           flexDirection: 'row'
                       }}
                   >
                       <View style={{width: '30%', justifyContent: 'center', alignItems: 'center'}}>
                           <Image
                               style={{width: '90%', height: 100, resizeMode: 'contain'}}
                               source={{uri: item.unitImage}}
                           />
                       </View>
                       <View style={{
                           width: '70%',
                           paddingHorizontal: 10
                       }}>
                           <View style={{width: '100%', flexDirection: 'row', alignItems: 'center'}}>
                               <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>Nama Mobil :</Text>
                               <Text style={{fontSize: 14, color: '#000'}}> {item.title}</Text>
                           </View>
                           <View style={{width: '100%', flexDirection: 'row', alignItems: 'center'}}>
                               <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>Total KM :</Text>
                               <Text style={{fontSize: 14, color: '#000'}}> {item.totalKM}</Text>
                           </View>
                           <View style={{width: '100%', flexDirection: 'row', alignItems: 'center'}}>
                               <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>Harga Mobil :</Text>
                               <Text style={{fontSize: 14, color: '#000'}}> {convertCurrency(item.harga,'Rp. ')}</Text>
                           </View>
                       </View>
                   </TouchableOpacity>
               )}
           />
                   </ScrollView>
           <TouchableOpacity
               style={{
                   position: 'absolute',
                   bottom: 30,
                   right: 10,
                   width: 40,
                   height: 40,
                   borderRadius: 20,
                   backgroundColor: 'red',
                   justifyContent: 'center',
                   alignItems: 'center'
               }}
               onPress={() => navigation.navigate('AddData')}
           >
               <Icon
                   name="plus"
                   size={20}
                   color="#fff"
               />
           </TouchableOpacity>
       </View>

  )
}

export default Home